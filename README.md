# INNOCV

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.12. and Bootstrap 4 for INNOCV as UX Exercise.

The backend has been developed with Firebase.

## 🚀 Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Mobile Mockup
![Landing](https://github.com/anamontiaga/innocv-crud-app/blob/master/mockup/employeeAdd.png)
![Editor](https://github.com/anamontiaga/innocv-crud-app/blob/master/mockup/employeesList.png)




Please support this project by simply putting a Github star ⭐. 🙏 Thanks
